pdvApp = angular.module('pdvApp', ['ngSanitize', 'ngRoute', 'ngResource', 'PdvCtrl', 'pdvService','ngCookies'])

pdvApp.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
  $routeProvider.when '/pointsVentes', {templateUrl: 'webroot/js/angular/templates/pdv.tpl.html', controller: 'pdvIndexCtrl'}
  $routeProvider.when '/caisse', {templateUrl: 'webroot/js/angular/templates/caisse.tpl.html', controller: 'pdvIndexCtrl'}
  $routeProvider.when '/gerer/:idpdv', {templateUrl: 'webroot/js/angular/templates/gerer_pdv.tpl.html', controller: 'majPdvCtrl'}
  $routeProvider.when '/stock', {templateUrl: 'webroot/js/angular/templates/stock.tpl.html', controller: 'stockArticles'}
  $routeProvider.otherwise('/pointsVentes')


]