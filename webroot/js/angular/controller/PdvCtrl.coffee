pdvCtrl = angular.module 'PdvCtrl', []
pdvCtrl.controller('pdvIndexCtrl', ['$scope', 'pdvFac', '$location', '$rootScope', '$cookieStore', ($scope, pdvFac, $location,$rootScope, $cookieStore) ->
  $scope.pdvAll = {}
  pdvFac.get (response) ->
    $scope.pdvAll = response
    #    construire un tab avec les data pour le graph
    $scope.response = response
    $scope.length = response.pointsVentes.length
    $scope.maj = $location.search()

  $scope.demarrerCaisse = (e) ->
    el = $(e.currentTarget);
    resId = el.find($('.hidden')).html();
    $cookieStore.put('cookieIdPdv',resId);
    $rootScope.idPdv = resId
    $location.path('/caisse')
])

pdvCtrl.controller('caisseCtrl', ['$scope', '$location', '$http', '$rootScope','$cookieStore', ($scope, $location, $http,$rootScope, $cookieStore) ->
  nombre = ''
  remiseP = 0.00
  newRemise = 0.00
  espece = 0.00
  rendu = 0.00
  prixQte = 0
  i = 0
  j=0
  tabNb = []
  nb =0
  $scope.leTotal1Carte = 0
  $scope.leTotal1Espece = 0
  $scope.leTotalCheque = 0
  $scope.leTotalEspeceSup = 0
  $scope.leTotalPlusieursCarte = 0
  $scope.leTotalPlusieursEspece = 0

  tableau = []
  tabMoyPaiement = []
  sommeCarte = 0.00
  tabArticles = []
  $scope.sousTotal = 0.00
  $scope.newQte = 1
  $scope.bonReduction = 0.00

  $scope.modInfo = "Caisse prête"
  $scope.goToStock = ->
    $location.path('/stock')


  $scope.entreeClavier = (nbr) ->
    nombre = nombre + nbr
    $scope.modSaisie = nombre
    false

    $scope.efface = ->
      nombre = nombre.substring(0,nombre.length-1);
      $scope.modSaisie = nombre

  $scope.afficheListe = (Cb = null)->

    if Cb == null
      codebarre = $scope.modSaisie
    else
      codebarre = Cb
    $http
      method: 'POST',
      url: 'api/article/'+codebarre+'.json',
      data:
        idPdv: $cookieStore.get('cookieIdPdv')
    .success (data) ->
        $scope.modInfo = "Caisse prête"

        if data['articles'] != undefined
          if $scope.newQte == 1

            tableau[j] = data
            tableau[j]['articles'].quantite = $scope.newQte
            tableau[j]['articles'].rang = j

            $scope.panier = tableau



            $scope.modSaisie = ""
            nombre = ''
            $scope.sousTotal = $scope.sousTotal + data.articles.prix


            j++
            $scope.initAucunArticle()
          else
            prixQte = $scope.newQte*data.articles.prix

            tableau[j] = data
            tableau[j]['articles'].prix = prixQte
            tableau[j]['articles'].quantite = $scope.newQte
            tableau[j]['articles'].rang = j

            $scope.panier = tableau


            $scope.modSaisie = ""
            nombre = ''
            $scope.sousTotal = $scope.sousTotal + prixQte

            $scope.newQte = 1
            $scope.modSaisie = ""
            j++
            $scope.initAucunArticle()
        else
          $scope.modInfo = "Article inconnu dans le stock"
          $scope.modSaisie = ""
        false


        $scope.remisePourcent = ->
          if $scope.modSaisie != '' and $scope.modSaisie <= 100
            remiseP = $scope.modSaisie
            newRemise = data.articles.prix * (remiseP/100)

            tableau[j-1]['articles'].remisePE = newRemise
            tableau[j-1]['articles'].remiseP = remiseP


            $scope.modSaisie = ""
            $scope.sousTotal = $scope.sousTotal - newRemise

            nombre = ''
          else
            nombre = ''
            $scope.modSaisie = ""
            $scope.modInfo = "Montant du pourcentage de remise trop élevé ou innexistant"
          false

        $scope.remiseEuros = ->
          if $scope.modSaisie != '' and $scope.modSaisie <= data.articles.prix and $scope.modSaisie <= $scope.sousTotal
            remiseE = $scope.modSaisie
            tableau[j-1]['articles'].remiseE = remiseE

            $scope.modSaisie = ""
            $scope.sousTotal = $scope.sousTotal - remiseE

            nombre = ''
          else
            nombre = ''
            $scope.modSaisie = ""
            $scope.modInfo = "Montant de la remise trop élevé ou innexistant"
          false


    .error (data) ->
      nombre = ''

  $scope.quantite = ->
    if $scope.modSaisie != ''
      quantite = $scope.modSaisie
      $scope.newQte = parseInt(quantite)
      $scope.modSaisie = ""
      $scope.modInfo = "Quantité :"+$scope.newQte
      nombre = ''
    else
      nombre = ''
      $scope.modSaisie = ""
      $scope.modInfo = "Quantité incorrecte"
    false

  $scope.bonReduc = ->
      if $scope.modSaisie != '' and $scope.modSaisie <= $scope.sousTotal
        $scope.bonReduction = $scope.modSaisie

        tableau[j-1]['articles'].bonReduc = $scope.bonReduction

        $scope.sousTotal = $scope.sousTotal - $scope.bonReduction

        $scope.modSaisie = ""
        nombre = ''
        $scope.bonReduction = 0
      else
        nombre = ''
        $scope.modSaisie = ""
        $scope.modInfo = "Montant du bon de réduction trop élevé ou innexistant"

      false

  $scope.carte = ->
    if $scope.modSaisie == ''
      $scope.modInfo = "Paiement par carte en cours..."



      tableau[j-1]['articles'].carte = $scope.sousTotal


      $scope.leTotal1Carte = $scope.sousTotal


      $scope.sousTotal = 0
      $scope.initMoyPaiement()
    else
      sommeCarte = $scope.modSaisie
      $scope.sousTotal = $scope.sousTotal - sommeCarte

      $scope.leTotalPlusieursCarte = parseInt(sommeCarte)

      tableau[j-1]['articles'].carte = sommeCarte

      $scope.modInfo = "Paiement carte "+sommeCarte+"€"

      nombre = ''
      $scope.modSaisie = ""
    false

  $scope.cheque = ->
    $scope.modInfo = "Paiement par chèque en cours..."


    tableau[j-1]['articles'].cheque = $scope.sousTotal


    $scope.leTotalCheque = $scope.sousTotal
    $scope.sousTotal = 0
    $scope.initMoyPaiement()
    false

  $scope.espece = ->
    if $scope.modSaisie == ''
      $scope.modInfo = "Paiement espèce : "+$scope.sousTotal+"€"


      tableau[j-1]['articles'].espece = $scope.sousTotal

      $scope.leTotal1Espece = $scope.sousTotal
      $scope.sousTotal = 0
      $scope.initMoyPaiement()
    else
      espece = $scope.modSaisie
      if espece > $scope.sousTotal
        rendu = espece - $scope.sousTotal

        tableau[j-1]['articles'].espece = espece
        tableau[j-1]['articles'].rendu = rendu

        $scope.modInfo = "A rendre : "+rendu.toFixed(2)+"€"
        nombre = ''
        $scope.modSaisie = ""


        $scope.leTotalEspeceSup = $scope.sousTotal
        $scope.sousTotal = 0
        $scope.initMoyPaiement()
      else
        $scope.sousTotal = $scope.sousTotal - espece

        $scope.modInfo = "Paiement espèces "+espece+"€"

        $scope.leTotalPlusieursEspece = parseInt(espece)

        tableau[j-1]['articles'].espece = espece
        nombre = ''
        $scope.modSaisie = ""
    false

  $scope.initBoutonTerminer = ->
    $('.terminerVente').prop('disabled', true)
    $('.btnEntrer').prop('disabled', false)
    $('.btnCheque').prop('disabled', true)
    $('.btnCarte').prop('disabled', true)
    $('.btnEspece').prop('disabled', true)
    $('.btnBonReduc').prop('disabled', true)
    $('.btnRemiseE').prop('disabled', true)
    $('.btnRemiseP').prop('disabled', true)

  $scope.initMoyPaiement = ->
    $('.terminerVente').prop('disabled', false)
    $('.btnEntrer').prop('disabled', true)
    $('.btnCheque').prop('disabled', true)
    $('.btnCarte').prop('disabled', true)
    $('.btnEspece').prop('disabled', true)
    $('.btnBonReduc').prop('disabled', true)
    $('.btnRemiseE').prop('disabled', true)
    $('.btnRemiseP').prop('disabled', true)

  $scope.initAucunArticle = ->
    $('.btnBonReduc').prop('disabled', false)
    $('.btnRemiseE').prop('disabled', false)
    $('.btnRemiseP').prop('disabled', false)
    $('.btnCheque').prop('disabled', false)
    $('.btnCarte').prop('disabled', false)
    $('.btnEspece').prop('disabled', false)

  $scope.terminerVente = ->
    $scope.initBoutonTerminer()
    $scope.modInfo = "Vente terminée, client suivant..."


    tableau = []
    false

    for tabNb in $scope.panier
      if tabNb['articles'].length != 0
        nb = nb + tabNb['articles'].quantite

    $scope.aaa = $scope.leTotal1Carte + $scope.leTotal1Espece + $scope.leTotalCheque + $scope.leTotalEspeceSup + $scope.leTotalPlusieursCarte + $scope.leTotalPlusieursEspece
    console.log($scope.leTotal1Carte)
    console.log($scope.leTotal1Espece)
    console.log($scope.leTotalCheque)
    console.log($scope.leTotalEspeceSup)
    console.log($scope.leTotalPlusieursCarte)
    console.log($scope.leTotalPlusieursEspece)

    $http
      method: 'POST',
      url: 'api/add.json',
      headers:
        'Content-Type': 'application/json',
      data:
        articles : $scope.panier,
        total : $scope.aaa,
        pointsVenteId : $cookieStore.get('cookieIdPdv'),
        nb : nb


    .success (data) ->
      $scope.panier = []
      tableau = []
      $scope.ArticlesReturn = []
      j = 0

      $scope.leTotal1Carte = 0
      $scope.leTotal1Espece = 0
      $scope.leTotalCheque = 0
      $scope.leTotalEspeceSup = 0
      $scope.leTotalPlusieursCarte = 0
      $scope.leTotalPlusieursEspece = 0




  $scope.rechercherArticle = ->
    $http
      method: 'POST',
      url: 'api/search/article.json',
      data:
        intitule: $scope.intituleSearch,
        idPdv: $cookieStore.get('cookieIdPdv')


    .success (data) ->

      $scope.ArticlesReturn = data



  $scope.ajoutPanier = (e) ->
    el = $(e.currentTarget)
    resCb = el.find($('.hidden')).html()
    $scope.afficheListe(resCb)


  $scope.deleteArticle = (nbrRang) ->
    $scope.sousTotal = $scope.sousTotal - tableau[nbrRang]['articles'].prix
    if tableau[nbrRang]['articles'].remisePE
      $scope.sousTotal = $scope.sousTotal + tableau[nbrRang]['articles'].remisePE

    if tableau[nbrRang]['articles'].remiseE
      $scope.sousTotal = $scope.sousTotal + tableau[nbrRang]['articles'].remiseE

    tableau[nbrRang]['articles'] = []


    if $scope.sousTotal < 0
      $scope.sousTotal = 0




])

#controller pour la mise à jour du pdv
pdvCtrl.controller('majPdvCtrl', ['$scope', '$routeParams', 'pdvByIdFac', '$location', ($scope, $routeParams, pdvByIdFac, $location) ->
  pdvByIdFac.get {id:$routeParams.idpdv}, (data) ->
    $scope.pdv = data.pointsVentes
  $scope.maj = ->
    pdvByIdFac.save($scope.pdv)
    $location.path '/pointsVentes'
    $location.search 'message', 'Les informations de votre point de vente ont été mises à jour.'




])
