var pdvApp = angular.module('pdvApp');
pdvApp.controller('stockArticles', ['$scope', '$http', '$location', '$rootScope', '$cookieStore', function ($scope, $http, $location, $rootScope, $cookieStore) {
    $scope.articles = [];
    $scope.visibiliteInputQuantite = true;
    $scope.visilibiteBtnValider = true;
    $scope.visibiliteBtnModifier = false;
    $scope.titreForm = "Ajouter un article";
    $scope.visibiliteMessInfo = false;
    $scope.messInfo = '';

    //Methode de redirection vers la caisse enregistreuse
    $scope.goToCaisse = function(){
        $location.path('/caisse');
    };

    //Methode qui affiche les articles dans le tableau du stock
    $scope.afficheStock = function(){
        $http({
            method: 'GET',
            url: "api/articles/stock/"+$cookieStore.get('cookieIdPdv')+".json"
        })
            .success(function (data) {
                angular.forEach(data, function(value) {
                    $scope.articles = value;
                },data);
                //Methode d'ajout d'un article dans le stock
                $scope.ajoutArticle = function(){
                    // On test si aucun des champs n'est vide
                    if($scope.intitule == undefined || $scope.reference == undefined || $scope.code_barre == undefined || $scope.prix == undefined || $scope.categorie == undefined || $scope.quantite == undefined || $scope.tva == undefined){
                        $scope.visibiliteMessInfo = true;
                        $scope.messInfo = "L'article n'a pu etre ajouter (vous devez renseigner tous les champs)";
                        return;
                    }
                    $http({
                        method: 'POST',
                        url: "api/add/articleForPdv/"+$cookieStore.get('cookieIdPdv')+".json",
                        data:{
                            intitule : $scope.intitule,
                            reference : $scope.reference,
                            code_barre : $scope.code_barre,
                            prix : $scope.prix,
                            categorie : $scope.categorie,
                            tva : $scope.tva,
                            quantite : $scope.quantite
                        }
                    })
                        .success(function (data) {
                            $scope.afficheStock();
                            $scope.visibiliteMessInfo = true;
                            $scope.messInfo = 'Article ajouté au stock !';
                            $scope.intitule = '';
                            $scope.reference ='';
                            $scope.code_barre = '';
                            $scope.prix = '';
                            $scope.categorie = '';
                            $scope.quantite = '';
                        })
                        .error(function (data, status, headers, config) {
                            // Une erreur est survenue
                            $scope.error = status;
                        });
                };
                // Methode de modification de la quantité d'un article
                $scope.modifQte = function(e){
                    var el, idArticle;
                    el = $(e.currentTarget);
                    idArticle = el.find($('.hidden')).html();

                    var newQuantite = prompt("Entrer la nouvelle quantité", "");
                    if (newQuantite != null) {
                        if(newQuantite < 0){
                            $scope.visibiliteMessInfo = true;
                            $scope.messInfo = 'La quantité ne peut être inférieur à 0';
                            return;
                        }
                        $http({
                            method: 'POST',
                            url: "api/updateQuantite.json",
                            data:{
                                idArticle : idArticle,
                                idPdv : $cookieStore.get('cookieIdPdv'),
                                newQuantite : newQuantite
                            }
                        })
                            .success(function (data) {
                                $scope.afficheStock();
                                $scope.visibiliteMessInfo = true;
                                $scope.messInfo = 'Quantité modifiée';
                                $scope.intitule = '';
                                $scope.reference ='';
                                $scope.code_barre = '';
                                $scope.prix = '';
                                $scope.categorie = '';
                                $scope.quantite = '';
                            })
                            .error(function (data, status, headers, config) {
                                // Une erreur est survenue
                                $scope.error = status;
                            });
                    }
                };
                // Methode pour modifier un article du stock
                $scope.modifArticle = function(e){
                    $scope.visibiliteInputQuantite = false;
                    $scope.visilibiteBtnValider = false;
                    $scope.visibiliteBtnModifier = true;
                    $scope.titreForm = "Modifier un article";
                    var el, idArticle;
                    el = $(e.currentTarget);
                    idArticle = el.find($('.hidden')).html();
                    $rootScope.idDeLarticle = idArticle;

                    // Scroll en bas de la page
                    window.scrollTo(0,document.body.scrollHeight);

                    angular.forEach($scope.articles, function(tab) {
                        if(tab[0].id == idArticle){
                            //$('#inputQuantite').prop('disabled', true);
                            $scope.intitule = tab[0].intitule;
                            $scope.reference = tab[0].reference;
                            $scope.code_barre = tab[0].code_barre;
                            $scope.prix = tab[0].prix;
                            $scope.categorie = tab[0].categorie;
                            if(tab[0].tva == 20){
                                $("#btnRadio2").prop('checked', true);
                                $("#btnRadio1").prop('checked', false);
                            }
                            else{
                                $("#btnRadio1").prop('checked', true);
                                $("#btnRadio2").prop('checked', false);
                            }
                        }
                    },$scope.articles);
                };
                // Methode de validation pour la modification d'un article du stock
                $scope.validModifArticle = function(){
                    $scope.visibiliteInputQuantite = true;
                    $scope.visilibiteBtnValider = true;
                    $scope.visibiliteBtnModifier = false;
                    $scope.titreForm = "Ajouter un article";
                    $http({
                        method: 'POST',
                        url: "api/updateArticle.json",
                        data: {
                            idArticle : $rootScope.idDeLarticle,
                            intitule : $scope.intitule,
                            reference : $scope.reference,
                            code_barre : $scope.code_barre,
                            prix : $scope.prix,
                            categorie : $scope.categorie,
                            tva : $scope.tva
                        }
                    })
                        .success(function (data) {
                            //console.log(data);
                            $scope.afficheStock();
                            $scope.visibiliteMessInfo = true;
                            $scope.messInfo = "Modification de l'article effectuée";
                            $scope.intitule = '';
                            $scope.reference ='';
                            $scope.code_barre = '';
                            $scope.prix = '';
                            $scope.categorie = '';
                            $scope.quantite = '';
                        })
                        .error(function (data, status, headers, config) {
                            // Une erreur est survenue
                            $scope.error = status;
                            $scope.afficheStock();
                            $scope.visibiliteMessInfo = true;
                            $scope.messInfo = "Modification de l'article effectuée";
                            $scope.intitule = '';
                            $scope.reference ='';
                            $scope.code_barre = '';
                            $scope.prix = '';
                            $scope.categorie = '';
                            $scope.quantite = '';
                        });
                };
                // Methode de suppression d'un article du stock
                $scope.supprimeArticle = function(e){
                    var el, idArticle;
                    el = $(e.currentTarget);
                    idArticle = el.find($('.hidden')).html();

                    $http({
                        method: 'POST',
                        url: "api/deleteArticle.json",
                        data:{
                            idArticle : idArticle
                        }
                    })
                        .success(function (data) {
                            $scope.afficheStock();
                            $scope.visibiliteMessInfo = true;
                            $scope.messInfo = "L'article a été effacé !";
                        })
                        .error(function (data, status, headers, config) {
                            // Une erreur est survenue
                            $scope.error = status;
                        });
                };
                // Methode de Reset : Vide tous les champs du formulaire
                $scope.annulerReset = function(){
                    $scope.visibiliteInputQuantite = true;
                    $scope.visilibiteBtnValider = true;
                    $scope.visibiliteBtnModifier = false;
                    $scope.titreForm = "Ajouter un article";
                    $scope.intitule = '';
                    $scope.reference ='';
                    $scope.code_barre = '';
                    $scope.prix = '';
                    $scope.categorie = '';
                    $scope.quantite = '';
                };
            })
            .error(function (data, status, headers, config) {
                // Une erreur est survenue
                $scope.error = status;
            });
    };

    $scope.afficheStock();
}]);