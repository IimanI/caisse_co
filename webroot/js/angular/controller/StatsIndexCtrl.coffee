statsCtrl = angular.module 'StatsIndexCtrl', ['ui.bootstrap']
#controller pour le graph total des ventes
statsCtrl.controller('TicketsCtrl', ['$scope', '$routeParams', '$location', '$rootScope', '$http', ($scope, $routeParams, $location, $rootScope, $http) ->

  $scope.init = ->
    $scope.ticketAll = {}

    $http
      method: 'GET',
      url: '/caisse_co/api/pointsVentes/'+$routeParams.idpdv+'/tickets.json',
      headers:
        'Content-Type': 'application/x-www-form-urlencoded',
    .success (data) ->
      $scope.ticketAll = data
    #    construire un tab avec les data pour le graph
      lineData = []
      infoData = []

      $scope.total = 0
      nbVente = 0

      $scope.lineData = {}
      $scope.info = {}
      for tabdata in data.tickets
        lineData.push {y: tabdata.created.substring(0,10), total: tabdata.total}
        infoData.push {date: tabdata.created.substring(0,10), pdvId: tabdata.pointsVente_id, tot: tabdata.total}
        $scope.total = $scope.total + tabdata.total
        nbVente++
      $scope.nbVente = nbVente
      $scope.lineData = lineData
      $scope.info = infoData
      #    initialiser l'onglet à afficher par default ie graphique



  $scope.chercherPeriode = ->
    $http
      method: 'POST',
      url: '/caisse_co/api/pointsVentes/'+$routeParams.idpdv+'/ticketsByPeriode.json',
      data:
        de : $scope.dt,
        a : $scope.dt2
    .success (data) ->
      $scope.ticketAll = data
      #    construire un tab avec les data pour le graph
      lineData = []
      infoData = []

      $scope.total = 0
      nbVente = 0


      $scope.lineData = {}
      $scope.info = {}
      for tabdata in data.tickets
        lineData.push {y: tabdata.created.substring(0,10), total: tabdata.total}
        infoData.push {date: tabdata.created.substring(0,10), pdvId: tabdata.pointsVente_id, tot: tabdata.total}
        $scope.total = $scope.total + tabdata.total
        nbVente++
      $scope.nbVente = nbVente
      $scope.lineData = lineData
      $scope.info = infoData

  $scope.tab = 1
  $scope.init()

  $scope.test = ->

    $scope.open2 = ($event) ->
      $event.preventDefault()
      $event.stopPropagation()

      $scope.opened2 = true

    $scope.open = ($event) ->
      $event.preventDefault()
      $event.stopPropagation()

      $scope.opened = true




  $scope.test()

])

