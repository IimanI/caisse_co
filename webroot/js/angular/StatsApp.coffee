statsApp = angular.module('statsApp', ['stpa.morris', 'ngSanitize', 'ngRoute', 'ngResource', 'StatsIndexCtrl', 'statsService'])

statsApp.config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
  $routeProvider.when '/statistiques/:idpdv', {templateUrl: '/caisse_co/webroot/js/angular/templates/statsIndex.tpl.html', controller: 'TicketsCtrl'}
  $routeProvider.otherwise('/statistiques')


]