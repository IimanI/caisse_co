statsService = angular.module 'statsService', ['ngResource']
statsService.factory('tickets', ['$resource', ($resource) ->
  $resource '/caisse_co/api/pointsVentes/:id/tickets.json', {id: '@id'}, {}
])
statsService.factory('articles', ['$resource', ($resource) ->
  $resource '/caisse_co/api/pointsVentes/articles.json', {}, {query: {method:'GET', isArray:false}}
])