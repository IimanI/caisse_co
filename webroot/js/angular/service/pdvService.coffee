pdvService = angular.module 'pdvService', ['ngResource']
pdvService.factory('pdvFac', ['$resource', ($resource) ->
  $resource 'api/pointsVentes.json', {}, {query: {method:'GET', isArray:false}}
])
pdvService.factory('pdvByIdFac', ['$resource', ($resource) ->
  $resource 'api/pointsVentes/:id.json', {id: '@id'}, {}
])