# Comment installer caisse co à partir du CD-Rom

## Installation

1. Copier le contenu du CD-Rom à la racine de votre serveur web

## Configuration

1. Renomer le fichier `config/app.default.php` en `config/app.php`
2. Editer le fichier `config/app.php` 
3. Configurer 'Datasources' avec les informations pour accéder à votre base de données
4. Modifier 'salt' avec la valeur de votre choix
5. Charger la base de données grâce au fichier `caisse_co.sql`

C'est la configuration la plus simple pour avoir un site fonctionnel, pour une configuration plus avancée:

[configuration de cake](http://book.cakephp.org/3.0/fr/development/configuration.html)
