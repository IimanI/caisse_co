<?php
/**
 * page d'acccueil pour les stats
 */

//config du layout
$this->layout = 'appli_layout';
$this->assign('title', 'Statistiques' )
?>

<section ng-app="statsApp">
    <article ng-view></article>
</section>