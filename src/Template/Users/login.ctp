<header id="connexion">
    <section class="container form" id="login">
        <h1>Connectez-vous</h1>
        <?= $this->Flash->render('auth') ?>
        <?= $this->Form->create() ?>
        <div class="field-wrap">
            <?= $this->Form->input('username', ['autocomplete' => 'off']) ?>
        </div>
        <div class="field-wrap">
            <?= $this->Form->input('password', ['autocomplete' => 'off']) ?>
        </div>
        <?= $this->Form->button('Valider', ['class' => 'btn btn-primary btn-lg']); ?>
        <?= $this->Form->end() ?>
    </section>
</header>
