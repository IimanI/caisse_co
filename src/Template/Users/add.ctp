<header id="inscription">
    <section class="container form" id="signup">
        <h1>Inscrivez-vous</h1>
        <?= $this->Form->create($user) ?>
        <div class="top-row">
            <div class="field-wrap">
                <?= $this->Form->input('nom', ['autocomplete' => 'off']) ?>
            </div>
            <div class="field-wrap">
                <?= $this->Form->input('prenom', ['autocomplete' => 'off']) ?>
            </div>
        </div>
        <div class="field-wrap">
            <?= $this->Form->input('email', ['autocomplete' => 'off']) ?>
        </div>
        <div class="field-wrap">
            <?= $this->Form->input('username', ['autocomplete' => 'off']) ?>
        </div>
        <div class="field-wrap">
            <?= $this->Form->input('password', ['autocomplete' => 'off']) ?>
        </div>
        <?= $this->Form->button('Valider', ['class' => 'btn btn-primary btn-lg']); ?>
        <?= $this->Form->end() ?>
    </section>
</header>
