<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <meta name="viewport" content="width=device-width , initial-scale=1"/>
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/vitrine/style.css" rel="stylesheet" type="text/css">
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

    <!--Les css de bases a test-->
    <?= $this->Html->meta('icon') ?>

    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/base.css" rel="stylesheet" type="text/css">
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/cake.css" rel="stylesheet" type="text/css">

</head>
<body id="top">

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="<?php if($this->request->param('action') != 'index'){echo \Cake\Routing\Router::url(['_name'=>'home']);}?>#top">Caisse Co'</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li>
                    <a class="page-scroll" href="<?php if($this->request->param('action') != 'index'){echo \Cake\Routing\Router::url(['_name'=>'home']);}?>#fonctionnalites">Fonctionnalités</a>
                </li>
                <li>
                    <a class="page-scroll" href="<?php if($this->request->param('action') != 'index'){echo \Cake\Routing\Router::url(['_name'=>'home']);}?>#api">API</a>
                </li>
                <li>
                    <a class="page-scroll" href="<?php if($this->request->param('action') != 'index'){echo \Cake\Routing\Router::url(['_name'=>'home']);}?>#team">Team</a>
                </li>
                <li>
                    <a href="<?= \Cake\Routing\Router::url(['_name'=>'add']); ?>">Inscription</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                    $user = $this->Session->read('Auth.User');
                    if(!empty($user)) {
                        echo "<li class='dropdown'>
                                <a href=".\Cake\Routing\Router::url(['_name'=>'home'])." class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-user'></i> " .$user['username']." <b class='caret'></b></a>
                                <ul class='dropdown-menu'>
                                    <li>
                                        <a href=".\Cake\Routing\Router::url(['_name'=>'edit'])."><i class='fa fa-fw fa-user'></i> Profil</a>
                                    </li>
                                    <li>
                                      <a href=".\Cake\Routing\Router::url(['_name'=>'pointsVentes'])."><i class='fa fa-fw fa-envelope'></i> Mes points de ventes</a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                      <a href=". \Cake\Routing\Router::url(['_name'=>'logout'])."><i class='fa fa-fw fa-power-off'></i> Se deconnecter</a>
                                    </li>
                               </ul>
                              </li>";
                    }else{
                        echo "<li><a href=".\Cake\Routing\Router::url(['_name'=>'login']).">Se connecter</a></li>";
                    }
                ?>
            </ul>
        </div>
    </div>
</nav>
<?= $this->Flash->render() ?>
<div>
    <?= $this->fetch('content') ?>
</div>

<script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/jquery.js"></script>
<script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/bootstrap.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/vitrine/scrolling-nav.js"></script>
<script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/vitrine/formLogin.js"></script>

</body>
</html>
