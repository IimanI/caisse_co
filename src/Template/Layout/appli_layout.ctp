<?php
/**
 * layout pour la partie application
 */
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Application de caisse enregistreuse en ligne">
    <meta name="author" content="tHetEam">

    <title><?= $this->fetch('title'); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/bootstrap.css" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/appCss/style.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/appCss/sb-admin.css" rel="stylesheet" type="text/css">

    <!-- Morris Charts CSS -->
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/appCss/morris.css" rel="stylesheet" type="text/css">

    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/cake.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <section id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=\Cake\Routing\Router::url(['_name' => 'home'])?>">Caisse Co'</a>
                </div>

                <div class="navbar-ex1-collapse collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="<?= \Cake\Routing\Router::url(['_name'=>'pointsVentes']); ?>"><i class="fa fa-fw fa-table"></i> Points de ventes</a>
                        </li>
                        <li>
                            <a href="<?= \Cake\Routing\Router::url(['_name'=>'addPointsVentes']); ?>"><i class="fa fa-fw fa-edit"></i> Ajout point de vente</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $this->Session->read('Auth.User.username'); ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?=\Cake\Routing\Router::url(['_name' => 'edit'])?>"><i class="fa fa-fw fa-user"></i> Profil</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= \Cake\Routing\Router::url(['_name'=>'logout']); ?>"><i class="fa fa-fw fa-power-off"></i> Se deconnecter</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Brand and toggle get grouped for better mobile display -->

            <!-- Top Menu Items -->
        </nav>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </section>
    <!-- jQuery -->
    <script src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/bootstrap.js"></script>

    <!--ANGULAR Chemin a verifier-->
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular-route.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular-resource.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular-sanitize.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/ui-bootstrap-tpls-0.12.1.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular-cookies.js"></script>
    <!-- Morris Charts JavaScript -->
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/morris.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/raphael.min.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/lib/angular-morris-chart.min.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/StatsApp.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/service/statsService.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/PdvApp.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/service/pdvService.js"></script>

    <!--    statsApp JavaScript-->
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/controller/StatsIndexCtrl.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/controller/PdvCtrl.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/angular/controller/stockArticles.js"></script>
    <script type="text/javascript" src="<?= \Cake\Routing\Router::url(['_name' => 'home'])?>/webroot/js/vitrine/formLogin.js"></script>
</body>
</html>