<?php
/**
 * page d'acccueil pour les stats
 */

//config du layout
$this->layout = 'appli_layout';
$this->assign('title', 'Points de ventes' )
?>

<h1>Liste de mes points de ventes</h1>
<section ng-app="pdvApp">
    <article ng-view></article>
</section>