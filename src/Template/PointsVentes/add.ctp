<section class="panel panel-info">
    <div class="panel-heading">
        <h1>Ajouter un point de vente</h1>
    </div>
    <article class="panel-body">
        <div class="container form" id="ajouterPdv">
            <?= $this->Form->create($pdv) ?>
            <div class="field-wrap">
                <?= $this->Form->input('nom', ['autocomplete' => 'off'], ['class' => 'test']) ?>
            </div>
            <div class="field-wrap">
                <?= $this->Form->input('domaine_activite', ['autocomplete' => 'off']) ?>
            </div>
            <div class="field-wrap">
                <?= $this->Form->input('adresse', ['autocomplete' => 'off']) ?>
            </div>
            <?= $this->Form->button(('Ajouter'), ['class' => 'btn btn-primary btn-lg']); ?>
            <?= $this->Form->end() ?>
            <a href="pointsVentes" class="btn btn-danger btn-lg">Annuler</a>
        </div>
    </article>
</section>