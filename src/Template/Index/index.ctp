<header class="container-fluid">
    <div class="jumbotron col-md-offset-3 col-md-6">
        <h1>Caisse Co'</h1>
        <p>La Caisse Enregistreuse en ligne pour les commerçants branchés!</p>
        <p><a class="btn btn-primary btn-lg page-scroll" href="#fonctionnalites" role="button">En savoir plus</a></p>
    </div>
</header>
<section id="fonctionnalites" class="container-fluid bg-light-gray">
    <h2>Fonctionnalités</h2>
    <articles class="col-md-3">
        <img src="webroot/img/vitrine/logos/Cashier.png" class="img-responsive img-circle" alt="">
        <h4>Caisse enregistreuse</h4>
        <p>Avec la caisse enregistreuse, les articles peuvent être scannés par douchette ou encore entrés via leur code barre/référence. Toutes les fonctionnalités disponnibles avec une caisse physique comme la suppression d'un article de la liste de courses ou encore la mise en attente du ticket de caisse sont présente dans cette application.</p>
    </articles>
    <articles class="col-md-3">
        <img src="webroot/img/vitrine/logos/stock.png" class="img-responsive img-circle" alt="">
        <h4>Gestion des stocks</h4>
        <p>Chaque stock peut-être réapprovisionné. Une interface ergonomique permet de gérer les articles selon différents critères. Le commerçant a aussi la possibilité de scanner les produits (douchette) ou d'indiquer le code barre pour référencer les articles et leur quantité dans le stock.</p>
    </articles>
    <articles class="col-md-3">
        <img src="webroot/img/vitrine/logos/Shop.png" class="img-responsive img-circle" alt="">
        <h4>Gestion de point de vente</h4>
        <p>L’application permet de gérer un ou plusieurs points vente. Le commerçant peut ainsi centraliser la gestion de son business mais aussi ajouter un ou plusieurs utilisateurs pour l'accompagner dans son activité.</p>
    </articles>
    <articles class="col-md-3">
        <img src="webroot/img/vitrine/logos/stats.png" class="img-responsive img-circle" alt="">
        <h4>Satistiques de ventes</h4>
        <p>Des graphiques indiquent le volume total des ventes.Un classement des produits les plus vendus est aussi mis à la disposition du commerçant.</p>
    </articles>
</section>
<section id="api" class="container-fluid">
    <h2>Utilisation de l'API</h2>
    <p>L'API public apiArticles a été mise à disposition gratuitement par la team caisse Co', elle répertorie tous les articles contenus dans la base de donnée qui augmente au fur et à mesure grace aux client de l'application.</p>
    <h3>Format des données</h3>
    <p>Conformément à la RFC 4627, les résulats au format JSON sont encodés en UTF-8.</p>
    <h3>L'unique route, pour recuperer ces données est la suivante :</h3>
    <p class="text-muted">GET http://localhost/caisse_co/api/articles.json</p>
    <p>Exemple d'un réponse JSON :</p>
    <pre>{
    "articles": [
        {
            "article": {
                "intitule": "Betamethasone Dipropionate",
                "reference": 25156601955,
                "code_barre": 25156601958 81,
                "prix": 4.82,
                "categorie": "multimedia",
                "tva": 20.0

            }
        },
        {
            "article": {
                "intitule": Cephalexin,
                "reference": 78156601952,
                "code_barre": 93042645331 77,
                "prix": 125.45,
                "categorie": "telephonie",
                "tva": 5.5
            }
        }
    ]
}</pre>
</section>
<section id="team" class="container-fluid bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">La Team de ouf!!</h2>
                <h3 class="section-subheading text-muted">LP CISII projet tuteuré</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="webroot/img/vitrine/team/1.jpg" class="img-responsive img-circle" alt="">
                    <h4>Cyril JEROME</h4>
                    <p class="text-muted">Développeur/Intégrateur</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="webroot/img/vitrine/team/2.jpg" class="img-responsive img-circle" alt="">
                    <h4>Gaëtan BERNAGOU</h4>
                    <p class="text-muted">Développeur/Intégrateur</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="webroot/img/vitrine/team/3.jpg" class="img-responsive img-circle" alt="">
                    <h4>Victor KAYSER</h4>
                    <p class="text-muted">Développeur/Intégrateur</p>
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>