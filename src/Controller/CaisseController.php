<?php

namespace App\Controller;

class CaisseController extends AppController {

    /**
     * Affiche la page d'accueil statistiques
     */
    public function index(){
        $dataTest = [
            'couleur' => 'jaune',
            'type' => 'test',
            'nombre' => 45
        ];

        // passe à la vue les valeurs du tableau $dataTest.
        $this->set($dataTest);
        // appelle la vue index.ctp
        $this->render();
    }

    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent acceder a index
        if ($this->request->action === 'index') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}