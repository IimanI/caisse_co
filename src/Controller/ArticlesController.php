<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 04/03/2015
 * Time: 10:29
 */

namespace App\Controller;


use Cake\Collection\Collection;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;


/**
 * Class ArticlesController
 * @package App\Controller
 */
class ArticlesController extends AppController {

    // Autorise tout le monde a acceder a l'API qui retourne les articles.
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Envoi en json tous les articles vendus par un ou plusieurs points de vente
     */
    public function index(){
        $articles = $this->Articles->find('all');
        $this->set([
            'articles' => $articles,
            '_serialize' => ['articles']
        ]);
    }

    /**
     * @param $id
     * Retourne tout les articles vendus par le point de vente
     */
    public function articleVenduByPdv ($id) {
        //recup l'id du ou des pdv
        $pointsVentes = TableRegistry::get('PointsVentes')->get($id);
        //recup tous les tickets d'un pdv
        $idPdv = $pointsVentes['id'];
        // recup juste l'id grace au select
        $tickets = TableRegistry::get('Tickets')
            ->find()
            ->select(['id'])
            ->where(['pointsVente_id' => $idPdv]);
        $ticketsId = $tickets->extract('id')->toArray();
        for ($j =0; $j < count($ticketsId); $j++) {
            $id = $ticketsId[$j];
            //matching permet de recup les articles avec un id de ticket particulier
            $articles = TableRegistry::get('Articles')
                ->find()
                ->matching('Tickets', function ($q) use ($id) {return $q->where(['Tickets.id' => $id]);})
                ->select(['id', 'intitule'])
                ->countBy('intitule');
            //push dans un tab tous les articles qui correspondent
            $tabArticles[] = $articles->toArray();
        }
        $this->set([
            'articles' => $tabArticles,
            '_serialize' => ['articles']
        ]);
    }

    /**
     * @param $codebarre
     * Retourne l'article qui correspond au code barre passé en parametre
     */
    public function articleByCodeBarre($codebarre)
    {
        $idPdv = $this->request->data['idPdv'];

        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;


        $articles = $this->Articles->find()->where(['code_barre' => $codebarre])->first();


       $articleStock = TableRegistry::get('articles_stocks')->find()->where(['stock_id' => $idStock])->andWhere(['article_id' => $articles->id])->first();

        if( ($articleStock->stock_id == $idStock) and ($articleStock->article_id == $articles->id) ){
            $this->set([
                'articles' => $articles,
                '_serialize' => ['articles']
            ]);
        }
        else {
            $this->set([
                'articles' => 'article non référencé dans le stock',
                '_serialize' => ['articles']
            ]);
        }
        /*$this->set([
            'articles' => $articles,
            '_serialize' => ['articles']
        ]);*/



    }

    /**
     * Retourne les articles d'un point de vente repondant a une chaine de caractere (intitule)
     */
    public function search() {
        $intitule = $this->request->data['intitule'];
        $idPdv = $this->request->data['idPdv'];

        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;

        // Ici ce trouve tout les id d'article du point de vente
        $articles = TableRegistry::get('articles_stocks')->find()->where(['stock_id' => $idStock]);

        foreach($articles as $lol){
            $idArticle = $lol->article_id;
            $quantite = $lol->quantite;
            $tabQuantite[] = $quantite;
            $mesArticles = $this->Articles->find()->where(['id' => $idArticle])->andWhere(['intitule LIKE' => '%'.$intitule.'%'])->toArray();

            if($mesArticles != null){
                $mesArticles['quantite'] = $quantite;
                $tabArticles[] = $mesArticles;
            }
        }

        $this->set([
            'article' => $tabArticles,
            '_serialize' => ['article']
        ]);
    }

    /**
     * Creer un ticket de caisse ainsi que les associations ticket / article dans la table articles_tickets
     */
    public function add() {
        $tabArticles = $this->request->data['articles'];
        $total = $this->request->data['total'];
        $idPdv = $this->request->data['pointsVenteId'];
        $nb = $this->request->data['nb'];

        $datas = [
            'nb_articles' => $nb,
            'total' => $total,
            'pointsVente_id' => $idPdv,
            'client_id' => 2
        ];

        $ticket = TableRegistry::get('Tickets')->newEntity($datas);
        $result = TableRegistry::get('Tickets')->save($ticket);
        $lastId = $result->id;

        foreach($tabArticles as $tab) {
            $donnees = [
                'ticket_id' => $lastId,
                'article_id' => $tab['articles']['id']
            ];
            $ticketArticles = TableRegistry::get('articles_tickets')->newEntity($donnees);
            TableRegistry::get('articles_tickets')->save($ticketArticles);
        }

        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;

        foreach($tabArticles as $tabIdArticle) {
                $idArticle = $tabIdArticle['articles']['id'];
                $quantite = $tabIdArticle['articles']['quantite'];
                $ArticlesStock = TableRegistry::get('articles_stocks');
                $entity = $ArticlesStock->get([$idStock, $idArticle]);
                $oldquantity = $entity->quantite;

                $donneQuantite = [
                    'quantite' => $oldquantity - $quantite
                ];

                $entity = $ArticlesStock->patchEntity($entity, $donneQuantite);
                $ArticlesStock->save($entity);
        }

        $this->set([
            'tabarticles' => $this->request->data['articles'][0]['articles']['id'],
            '_serialize' => ['tabarticles']
        ]);
    }

    /**
     * @param null $idPdv
     * Cette API retourne tous les articles du stock d'un point de vente grace a l'id du point de vente
     */
    public function articlesParPointVenteId($idPdv){
        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;

        // Ici ce trouve tout les id d'article du point de vente
        $allArticlesStockQuery = TableRegistry::get('articles_stocks')->find()->where(['stock_id' => $idStock]);


        foreach($allArticlesStockQuery as $lol){
            $idArticle = $lol->article_id;
            $quantite = $lol->quantite;
            $tabQuantite[] = $quantite;
            $mesArticles = $this->Articles->find()->where(['id' => $idArticle])->toArray();
            $mesArticles['quantite'] = $quantite;
            $tabArticles[] = $mesArticles;
        }

        $this->set([
            'articles' => $tabArticles,
            '_serialize' => ['articles']
        ]);
    }

    /**
     * @param $idPdv
     * Ajout d'un article pour un point de vente donné !
     */
    public function addArticleForPdv($idPdv){
        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;

        $article = $this->request->data();

        $data = [
            'intitule' => $article['intitule'],
            'reference' => $article['reference'],
            'code_barre' => $article['code_barre'],
            'prix' => $article['prix'],
            'categorie' => $article['categorie'],
            'tva' => $article['tva']
        ];

        $newArticle = $this->Articles->newEntity($data);
        $res = $this->Articles->save($newArticle);
        $lastId = $res->id;

        $donnes = [
            'stock_id' => $idStock,
            'article_id' => $lastId,
            'quantite' => $article['quantite']
        ];

        $newArticle_stock = TableRegistry::get('articles_stocks')->newEntity($donnes);
        TableRegistry::get('articles_stocks')->save($newArticle_stock);

        $this->set([
            'articles' => $article,
            '_serialize' => ['articles']
        ]);
    }

    /**
     * Met a jour la quantité d'un article d'un point de vente
     */
    public function edit(){
        $datas = $this->request->data();
        $idPdv = $datas['idPdv'];

        // Avec l'id du point de vente on recupere l'id du stock
        $idStockQuery = TableRegistry::get('pointsVentes')->find()->select('stock_id')->where(['id' => $idPdv]);
        $idStock2 = $idStockQuery->first();
        // Ici ce trouve l'id du stock
        $idStock = $idStock2->stock_id;

        $articleStock = TableRegistry::get('articles_stocks');
        $entity = $articleStock->get([$idStock,$datas['idArticle']]);

        $donnees = [
            'quantite' => $datas['newQuantite']
        ];
        $entity = $articleStock->patchEntity($entity,$donnees);

        if($articleStock->save($entity)){
            $toto = 'Modification effectuee';
        }
        else
            $toto = 'Modification echouee';

        $this->set([
            'status' => $toto,
            '_serialize' => ['status']
        ]);
    }

    /**
     * Met a jour un article
     */
    public function updateArticle(){
        $datas = $this->request->data();

        $entity = $this->Articles->get($datas['idArticle']);

        $donnees = [
            'intitule' => $datas['intitule'],
            'reference' => $datas['reference'],
            'code_barre' => $datas['code_barre'],
            'prix' => $datas['prix'],
            'categorie' => $datas['categorie'],
            'tva' => $datas['tva']
        ];

        $entity = $this->Articles->patchEntity($entity,$donnees);

        if($this->Articles->save($entity)){
            $toto = 'Modification effectuee';
        }
        else
            $toto = 'Modification echouee';

        $this->set([
            'status' => $toto,
            '_serialize' => ['status']
        ]);
    }

    /**
     * Supprime un article
     */
    public function deleteArticle(){
        $datas = $this->request->data();

        $entity = $this->Articles->get($datas['idArticle']);

        if($this->Articles->delete($entity)){
            $toto = 'Suppression effectuee';
        }
        else
            $toto = 'Suppression echouee';

        $this->set([
            'status' => $toto,
            '_serialize' => ['status']
        ]);
    }

    /**
     * @param $user
     * @return bool
     * Permet d'autoriser les methodes aux utilisateurs connectés
     */
    public function isAuthorized($user)
    {
        if ($this->request->action === 'index') {
            return true;
        }
        if ($this->request->action === 'articleByCodeBarre') {
            return true;
        }
        if ($this->request->action === 'articleVenduByPdv') {
            return true;
        }
        if ($this->request->action === 'articlesParPointVenteId') {
            return true;
        }
        if ($this->request->action === 'addArticleForPdv') {
            return true;
        }
        if ($this->request->action === 'add') {
            return true;
        }
        if ($this->request->action === 'search') {
            return true;
        }
        if ($this->request->action === 'edit') {
            return true;
        }
        if ($this->request->action === 'updateArticle') {
            return true;
        }
        if ($this->request->action === 'deleteArticle') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}