<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class StockController extends AppController {

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index(){

        $pdv = TableRegistry::get('pointsVentes');
        $idStock = $pdv->find()->select(['stock_id'])->where(['user_id' => $_SESSION['Auth']['User']['id']]);

        $this->set([
            'articles' => $idStock,
            '_serialize' => ['articles']
        ]);
    }

    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent acceder a index
        if ($this->request->action === 'index') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}