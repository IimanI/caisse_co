<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;

class UsersController extends  AppController{

    /**
     * @param Event $event
     * Surcharge de la methode beforeFilter
     * Ici j'autorise tout le monde a acceder aux actions : add et logout
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'logout']);
    }

    public function index()
    {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id)
    {
        if (!$id) {
            throw new NotFoundException(__('utilisateur non valide'));
        }
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    /**
     * @return \Cake\Network\Response|void
     * Methode add qui sert a creer un compte utilisateur
     */
    public function add()
    {
        // J'utilise un layout perso
        $this->layout = 'vitrine';

        // Je recupere tous les username de ma bdd
        $allUsername = $this->Users->find()->select(['username']);

        // Je recupere toutes les adresses emails de ma bdd
        $allEmail = $this->Users->find()->select(['email']);

        // J'ajoute le role a la main en completant le tableau data
        $this->request->data['role'] = 'gerant';

        $user = $this->Users->newEntity($this->request->data);
        if ($this->request->is('post')) {

            // Je test si le username saisie par l'utilisateur est deja dans la bdd
            foreach ($allUsername as $username) {
                if($username->username == $this->request->data['username']){
                    $this->Flash->error(__("Username deja utilisé."));
                    return $this->redirect(['controller' => 'Users','action' => 'add']);
                    die();
                }
            }

            // Je test si l'adresse email saisie par l'utilisateur est deja dans la bdd
            foreach ($allEmail as $email) {
                if($email->email == $this->request->data['email']){
                    $this->Flash->error(__("L'adresse email est deja utilisé."));
                    return $this->redirect(['controller' => 'Users','action' => 'add']);
                    die();
                }
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__("L'utilisateur a été sauvegardé."));
                return $this->redirect(['controller' => 'Users','action' => 'login']);
            }
            $this->Flash->error(__("Impossible d'ajouter l'utilisateur."));
        }
        $this->set('user', $user);
    }

    /**
     * @return \Cake\Network\Response|void
     * Methode login qui sert a s'authentifier a l'aide de son couple username/password
     */
    public function login()
    {
        $this->layout = 'vitrine';

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'PointsVentes','action' => 'index']);
            }
            $this->Flash->error(__("Username ou mot de passe incorrect, essayez à nouveau."));
        }
    }

    /**
     * @return \Cake\Network\Response|void
     * Methode qui sert a modifier son compte
     */
    public function edit()
    {
        $this->layout = 'vitrine';
        $id= $_SESSION['Auth']['User']['id'];

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');
                return $this->redirect(['controller' => 'Index','action' => 'index']);
            } else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * @return \Cake\Network\Response|void
     * Methode qui sert a se deconnecter de sa session
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function isAuthorized($user)
    {
        if ($this->request->action === 'edit') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}