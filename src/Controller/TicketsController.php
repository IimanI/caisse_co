<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class TicketsController extends AppController{

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Tous les tickets d'un point de vente pour un utilisateur donné
     * Cette methode est utilisée dans angular pour les stats et retourne du Json
     * @param $id
     */
    public function index($id){


            $pointsVentes = TableRegistry::get('PointsVentes')->get($id);
            $idPdv = $pointsVentes['id'];
            $tickets = $this->Tickets->find()
                ->where(['pointsVente_id' => $idPdv])
                ->select(['total', 'created', 'pointsVente_id'])
                ->toArray();
            $this->set([
                'tickets' => $tickets,
                '_serialize' => ['tickets']
            ]);
        }


    /**
     * @param $id
     * Retourne les tickets de caisse pour une période défini
     */
    public function searchPeriode($id){
        $dataDates = $this->request->data;
        $de = $dataDates['de'];
        $de2 = substr($de,0,10);
        $de3 = $de2.' 00:00:00';
        $de4 = new \DateTime($de2);
        $a = $dataDates['a'];
        $a2 = substr($a,0,10);
        $a3 = $a2.' 23:59:59';
        $a4 = new \DateTime($a2);

        $pointsVentes = TableRegistry::get('PointsVentes')->get($id);
        $idPdv = $pointsVentes['id'];
        $tickets = $this->Tickets->find()
            ->where(['pointsVente_id' => $idPdv])
            ->andWhere(['created >=' => $de3])
            ->andWhere(['created <=' => $a3])
            ->select(['total', 'created', 'pointsVente_id'])
            ->toArray();

        $this->set([
            'tickets' => $tickets,
            '_serialize' => ['tickets']
        ]);
    }

    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent ajouter des articles
        if ($this->request->action === 'index') {
            return true;
        }
        if ($this->request->action === 'searchPeriode') {
            return true;
        }

        return parent::isAuthorized($user);
    }
}