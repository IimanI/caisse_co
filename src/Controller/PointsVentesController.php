<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class PointsVentesController extends AppController {

    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Affiche la page d'accueil des points de vente
     */
    public function index(){
        $pointsVentes = $this->PointsVentes->find()->where(['user_id' => $_SESSION['Auth']['User']['id']])->toArray();

        $this->set([
            'pointsVentes' => $pointsVentes,
            '_serialize' => ['pointsVentes']
        ]);
    }

    public function affichage(){
        // appelle la vue affichage.ctp
        $this->render();
    }

    /**
     * @return \Cake\Network\Response|void
     * Création d'un point de vente
     */
    public function add() {
        $this->layout = 'appli_layout';

        $this->request->data['user_id'] = $_SESSION['Auth']['User']['id'];
        $stocks = TableRegistry::get('Stocks')->newEntity();
        $stock = TableRegistry::get('Stocks')->save($stocks);
        $this->request->data['stock_id'] = $stock->id;
        $pdv = $this->PointsVentes->newEntity($this->request->data());
        if ($this->request->is('post')) {
            if ($this->PointsVentes->save($pdv)) {
                $this->Flash->success(__("Votre nouveau point de vente a bien été enregistré"));
                return $this->redirect(['controller' => 'PointsVentes','action' => 'index']);
            }
            $this->Flash->error(__("Impossible d'ajouter un point de vente supplémentaire."));
        }
        $this->set('pdv', $pdv);
    }

    /**
     * @param $id
     * Modification d'un point de vente
     */
    public function edit($id){
        $pdv = $this->PointsVentes->get($id);
        if ($this->request->is(['post', 'put'])) {
            $pdv = $this->PointsVentes->patchEntity($pdv, $this->request->data);
            if ($this->PointsVentes->save($pdv)) {
                $message = 'Les informations de votre point de vente ont été modifiées';
            } else {
                $message = 'Les informations de votre point de vente n\'ont pas été modifiées';
            }
        };
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }

    public function view($id) {
        $pdv = $this->PointsVentes->get($id);

        $this->set([
            'pointsVentes' => $pdv,
            '_serialize' => ['pointsVentes']
        ]);
    }

    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent ajouter des articles
        if ($this->request->action === 'index') {
            return true;
        }
        if ($this->request->action === 'add') {
            return true;
        }
        if ($this->request->action === 'edit') {
            return true;
        }
        if ($this->request->action === 'view') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}