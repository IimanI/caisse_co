<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 23/02/15
 * Time: 23:36
 */

namespace App\Controller;



use Cake\Event\Event;

class StatsController extends AppController {

    /**
     * affiche la page d'accueil statistiques
     */
    public function index(){


        $dataTest = [
            'couleur' => 'jaune',
            'type' => 'test',
            'nombre' => 45
        ];

        // passe à la vue les valeurs du tableau $dataTest.
        $this->set($dataTest);
        // appelle la vue index.ctp
        $this->render();
    }

    public function isAuthorized($user)
    {
        // Tous les utilisateurs enregistrés peuvent ajouter des articles
        if ($this->request->action === 'index') {
            return true;
        }
        return parent::isAuthorized($user);
    }
}