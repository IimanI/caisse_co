<?php

namespace App\Controller;

use Cake\Event\Event;

class IndexController extends AppController{

    public function beforeFilter(Event $event){
        // On dit a AuthComponent de ne pas exiger un login pour toutes les actions dans ce controller
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    /**
     * Affiche la page d'accueil du site vitrine
     */
    public function index(){
        $this->layout = 'vitrine';
        // appelle la vue index.ctp
        $this->render();
    }
}