<?php

/**
 * model pour la table tickets
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class TicketsTable extends Table {

    public function initialize(array $config){
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        /*$this->belongsToMany('Articles', [
            'targetForeignKey' => 'article_id',
            'foreignKey' => 'ticket_id',
            'joinTable' => 'articles_tickets'
        ]);
        $this->hasOne('PointsVente');
        $this->hasOne('client_id');*/
    }
}