<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class ArticlesTable extends Table {

    public function initialize(array $config)
    {
        $this->belongsToMany('Stocks', [
            'targetForeignKey' => 'stock_id',
            'foreignKey' => 'article_id',
            'joinTable' => 'articles_stocks'
        ]);
        $this->belongsToMany('Tickets', [
            'targetForeignKey' => 'ticket_id',
            'foreignKey' => 'article_id',
            'joinTable' => 'articles_tickets',
        ]);
    }
}