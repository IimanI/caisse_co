<?php
/**
 * model pour la table points_ventes
 */
namespace App\Model\Table;

use Cake\ORM\Table;

class PointsVentesTable extends Table {
    public function initialize(array $config)
    {
        $this->table('pointsVentes');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('Tickets');
    }
}