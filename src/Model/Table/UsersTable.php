<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table{

/*    public function beforeFilter(Event $event){
        // On dit a AuthComponent de ne pas exiger un login pour toutes les actions dans ce controller
        parent::beforeFilter($event);
        $this->Auth->allow();
    }*/

    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('PointsVentes');
    }

    public function validationDefault(Validator $validator){
        return $validator
            ->notEmpty('nom', "Un nom est nécessaire")
            ->notEmpty('prenom', "Un prénom est nécessaire")
            ->notEmpty('username', "Un username est nécessaire")
            ->notEmpty('email', "Une adresse email est nécessaire")
            ->notEmpty('password', "Un mot de passe est nécessaire");
    }
}