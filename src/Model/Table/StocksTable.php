<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class StocksTable extends Table {

    public function initialize(array $config)
    {
        $this->table('stocks');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('Articles');
        $this->hasOne('PointsVentes');
    }
}