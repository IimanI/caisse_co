<?php
/**
 * Created by PhpStorm.
 * User: Gaetan
 * Date: 03/03/2015
 * Time: 01:57
 */

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

class User extends Entity{

    protected $_accessible = [
        'username' => true,
        'password' => true,
        'role' => true,
        'nom' => true,
        'prenom' => true,
        'email' => true,
    ];


    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}