<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('Route');

Router::scope('/', function ($routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */

    /**
     * Route pour acceder à la page d'accueil du site vitrine
     */
    $routes->connect('/', ['controller' => 'Index', 'action' => 'index'], ['_name' => 'home']);
    $routes->connect('/add', ['controller' => 'Users', 'action' => 'add'], ['_name' => 'add']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login'], ['_name' => 'login']);
    $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout'], ['_name' => 'logout']);
    $routes->connect('/edit', ['controller' => 'Users', 'action' => 'edit'], ['_name' => 'edit']);

    /**
     * Route pour acceder à la page de stats
     */
    $routes->connect('/statistiques', ['controller' => 'Stats', 'action' => 'index'], ['_name' => 'statistiques']);

    /**
     * Route pour acceder à la caisse en ligne
     */
    $routes->connect('/caisse', ['controller' => 'Caisse', 'action' => 'index'], ['_name' => 'caisse']);

    /**
     * Route pour acceder à la liste des points de vente d'un utilisateur
     */
    $routes->connect('/pointsVentes', ['controller' => 'PointsVentes', 'action' => 'index'], ['_name' => 'pointsVentes']);

    /**
     * Route pour acceder à l'ajout d'un point de vente d'un utilisateur
     */
    $routes->connect('/ajoutPointsVentes', ['controller' => 'PointsVentes', 'action' => 'add'], ['_name' => 'addPointsVentes']);

    /**
     * Route pour acceder au stock de l'utilisateur
     */
    $routes->connect('/stock', ['controller' => 'Stock', 'action' => 'index'], ['_name' => 'stock']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `InflectedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('InflectedRoute');
});

/**
 * scope pour les api
 */
Router::scope('/api', function ($routesApi){
    //spécifie le format souhaité pour les résultats
    $routesApi->extensions(['json']);
    //liste tous les points de vente
    $routesApi->connect('/pointsVentes', ['controller' => 'PointsVentes', 'action' => 'index']);
    //liste tous les tickets d'un point de  vente pour un utilisateur donné
    $routesApi->connect('/pointsVentes/:id/tickets', ['controller' => 'Tickets', 'action' => 'index'],['pass' => ['id'], ['id' => '[0-9]+']]);

    $routesApi->connect('/pointsVentes/:id/ticketsByPeriode', ['controller' => 'Tickets', 'action' => 'searchPeriode'],['pass' => ['id'], ['id' => '[0-9]+']]);


    //liste les articles
    $routesApi->connect('/articles', ['controller' => 'Articles', 'action' => 'index']);
    // Liste tous les articles du stock d'un point de vente (grace a l'id du point de vente)
    $routesApi->connect('/articles/stock/:id', ['controller' => 'Articles',
                                                'action' => 'articlesParPointVenteId'],
                                                ['pass' => ['id'],
                                                ['id' => '[0-9]+']]);
    // Ajout d'un article a un point de vente
    $routesApi->connect('/add/articleForPdv/:id', ['controller' => 'Articles',
                                                'action' => 'addArticleForPdv'],
                                                ['pass' => ['id'],
                                                ['id' => '[0-9]+']]);
    // Modifie la quantite d'un article d'un point de vente
    $routesApi->connect('/updateQuantite', ['controller' => 'Articles', 'action' => 'edit', '_method' => 'POST']);
    // Modifie un article d'un point de vente
    $routesApi->connect('/updateArticle', ['controller' => 'Articles', 'action' => 'updateArticle', '_method' => 'POST']);
    // Supprimer un article d'un point de vente
    $routesApi->connect('/deleteArticle', ['controller' => 'Articles', 'action' => 'deleteArticle', '_method' => 'POST']);

    $routesApi->connect('/articles', ['controller' => 'Articles', 'action' => 'index']);
    // stock d'un point de vente
    $routesApi->connect('/stock', ['controller' => 'Stock', 'action' => 'index']);
    // articles vendu par un ou plusieurs points de vente
    $routesApi->connect('/pointsVentes/:id/articles', ['controller' => 'Articles', 'action' => 'articleVenduByPdv'], ['pass' => ['id'], ['id' => '[0-9]+']]);
    // Recherche un article par code barre
    $routesApi->connect('/article/:codebarre', ['controller' => 'Articles', 'action' => 'articleByCodeBarre'], ['pass' => ['codebarre']]);
    // Ajouter un article a la liste de vente en cours
    $routesApi->connect('/add', ['controller' => 'Articles', 'action' => 'add']);
    // modifier un pdv, on passe le param _method pour indiquer à cake l'action à choisir en fonction de la methode http
    $routesApi->connect('/pointsVentes/:id', ['controller' => 'PointsVentes', 'action' => 'edit', '_method' => 'POST'], ['pass' => ['id'],
        ['id' => '[0-9]+']]);
    // Retourne tout les point de vente grace a l'id de l'utilisateur
    $routesApi->connect('/pointsVentes/:id', ['controller' => 'PointsVentes', 'action' => 'view'], ['pass' => ['id'], ['id' => '[0-9]+']]);
    // Recherche les articles d'un point de vente grace a son intitule
    $routesApi->connect('/search/article', ['controller' => 'Articles', 'action' => 'search']);
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
