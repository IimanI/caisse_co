-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 02 Avril 2015 à 12:41
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `caisse_co`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(70) NOT NULL,
  `reference` varchar(70) NOT NULL,
  `code_barre` varchar(13) NOT NULL,
  `prix` decimal(8,2) DEFAULT NULL,
  `categorie` varchar(70) NOT NULL,
  `tva` decimal(3,1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_barre` (`code_barre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=168 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id`, `intitule`, `reference`, `code_barre`, `prix`, `categorie`, `tva`, `created`, `modified`) VALUES
(110, 'Samsung galaxy s3', '01', '1234567891230', '200.00', 'télephone', '20.0', NULL, NULL),
(111, 'Samsung galaxy s3 mini', '02', '1234567891231', '170.00', 'télephone', '0.0', NULL, NULL),
(112, 'Samsung galaxy s4', '03', '1234567891232', '350.00', 'télephone', '20.0', NULL, NULL),
(113, 'Samsung galaxy s5', '06', '1234567891233', '600.00', 'test', '20.0', NULL, NULL),
(125, 'iphone 4', '04', '1234567891234', '250.00', 'télephone', '20.0', NULL, NULL),
(127, 'iphone 5', '05', '1234567891235', '450.00', 'télephone', '20.0', NULL, NULL),
(130, 'iphone 5c blanc', '06', '1234567891236', '499.00', 'télephone', '20.0', NULL, NULL),
(131, 'iphone 5c vert', '07', '1234567891237', '499.00', 'télephone', '20.0', NULL, NULL),
(132, 'iphone 5c bleu', '08', '1234567891238', '499.00', 'télephone', '20.0', NULL, NULL),
(133, 'iphone 6', '09', '1234567891239', '699.00', 'télephone', '20.0', NULL, NULL),
(134, '', '', '', NULL, '', '20.0', NULL, NULL),
(136, 'iphone 6 plus', '10', '1234567891240', '850.00', 'télephone', '20.0', NULL, NULL),
(137, 'sony xperia z3', '11', '1234567891241', '400.00', 'télephone', '20.0', NULL, NULL),
(138, 'samsung galaxy s6 noir 64 Go', '12', '1234567891242', '989.00', 'télephone', '20.0', NULL, NULL),
(139, 'samsung galaxy note edge', '13', '1234567891243', '800.00', 'télephone', '20.0', NULL, NULL),
(140, 'HTC One m9', '14', '1234567891244', '749.00', 'télephone', '20.0', NULL, NULL),
(142, 'coque iphone 6', '15', '1234567891245', '45.00', 'accessoires', '20.0', NULL, NULL),
(146, 'Asus premium R510', '01', '1234567891246', '499.00', 'ordinateur', '20.0', NULL, NULL),
(147, 'Asus ROG G551', '1', '1234567891247', '969.00', 'ordinateur', '20.0', NULL, NULL),
(148, 'Asus ChromeBook', '8', '1234567891248', '199.00', 'ordinateur', '20.0', NULL, NULL),
(149, 'HP Pavillon Beats', '8', '1234567891249', '419.00', 'ordinateur', '20.0', NULL, NULL),
(150, 'HP 250 G2', '5', '1234567891250', '520.00', 'ordinateur', '20.0', NULL, NULL),
(151, 'Canon MX 395', '5', '1234567891251', '85.00', 'imprimante', '20.0', NULL, NULL),
(152, 'Hp office jetpro 6230', '8', '1234567891252', '65.00', 'imprimante', '20.0', NULL, NULL),
(153, 'Lenovo C365', '5', '1234567891253', '350.00', 'ordinateur', '20.0', NULL, NULL),
(154, 'GTA IV pc', '5', '1234567891254', '39.00', 'jeux pc', '20.0', NULL, NULL),
(155, 'Fifa 15', '6', '1234567891255', '59.00', 'jeux pc', '20.0', NULL, NULL),
(156, 'Call Of Duty 7', '6', '1234567891256', '59.00', 'jeux pc', '20.0', NULL, NULL),
(157, 'Mac book air 13"', '8', '1234567891257', '999.00', 'ordinateur', '20.0', NULL, NULL),
(158, 'coque samsung galaxy s6', 'fd', '1234567891210', '49.00', 'accessoire', '20.0', NULL, NULL),
(159, 'coque samsung galaxy s5', 'ds', '1234567891211', '35.00', 'accessoire', '20.0', NULL, NULL),
(160, 'coque iphone 5c', 'dsdsd', '1234567891212', '39.99', 'accessoire', '20.0', NULL, NULL),
(161, 'LG nexus 5', 'ok', '1234567891213', '330.00', 'télephone', '20.0', NULL, NULL),
(162, 'Motorola nexus 6 32 Go', 'ok', '1234567891214', '699.00', 'téléphone', '20.0', NULL, NULL),
(163, 'Motorola Nexus 6 64 Go', 'tre', '1234567891215', '850.00', 'téléphone', '20.0', NULL, NULL),
(164, 'Samsung Galaxy Alpha 32 Go', 'test', '1234567891216', '499.00', 'téléphone', '20.0', NULL, NULL),
(165, 'Logitech Souris sans fil 2564', 'zs', '1234567891258', '39.99', 'accessoire', '20.0', NULL, NULL),
(166, 'Clavier bluetooth Microsoft p74', 's', '1234567891259', '89.00', 'accessoire', '20.0', NULL, NULL),
(167, 'Ecran HP 22"', 'aa', '1234567891260', '79.00', 'accessoire', '20.0', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `articles_stocks`
--

CREATE TABLE IF NOT EXISTS `articles_stocks` (
  `stock_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`stock_id`,`article_id`),
  KEY `fk_stocks_has_articles_articles1` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `articles_stocks`
--

INSERT INTO `articles_stocks` (`stock_id`, `article_id`, `quantite`) VALUES
(35, 110, 50),
(35, 111, 79),
(35, 112, 47),
(35, 113, 50),
(35, 125, 50),
(35, 127, 28),
(35, 130, 23),
(35, 131, 36),
(35, 132, 65),
(35, 133, 97),
(35, 136, 58),
(35, 137, 34),
(35, 138, 25),
(35, 139, 55),
(35, 140, 74),
(35, 142, 98),
(35, 158, 20),
(35, 159, 30),
(35, 160, 42),
(35, 161, 60),
(35, 162, 50),
(35, 163, 30),
(35, 164, 50),
(37, 146, 19),
(37, 147, 11),
(37, 148, 25),
(37, 149, 22),
(37, 150, 12),
(37, 151, 10),
(37, 152, 20),
(37, 153, 20),
(37, 154, 11),
(37, 155, 19),
(37, 156, 19),
(37, 157, 20),
(37, 165, 54),
(37, 166, 78),
(37, 167, 56);

-- --------------------------------------------------------

--
-- Structure de la table `articles_tickets`
--

CREATE TABLE IF NOT EXISTS `articles_tickets` (
  `ticket_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`ticket_id`,`article_id`),
  KEY `fk_tickets_has_articles_articles1` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `articles_tickets`
--

INSERT INTO `articles_tickets` (`ticket_id`, `article_id`) VALUES
(161, 111),
(178, 111),
(175, 112),
(179, 112),
(180, 112),
(163, 127),
(160, 130),
(182, 130),
(159, 133),
(164, 133),
(165, 136),
(174, 136),
(181, 136),
(162, 138),
(176, 138),
(160, 142),
(164, 142),
(171, 146),
(171, 147),
(172, 147),
(173, 148),
(167, 149),
(168, 149),
(165, 150),
(169, 150),
(167, 154),
(166, 155),
(170, 156);

-- --------------------------------------------------------

--
-- Structure de la table `pointsventes`
--

CREATE TABLE IF NOT EXISTS `pointsventes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(70) NOT NULL,
  `domaine_activite` varchar(100) DEFAULT NULL,
  `adresse` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user_id`,`stock_id`),
  KEY `fk_pointsventes_users` (`user_id`),
  KEY `fk_pointsventes_stocks1` (`stock_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `pointsventes`
--

INSERT INTO `pointsventes` (`id`, `nom`, `domaine_activite`, `adresse`, `created`, `modified`, `user_id`, `stock_id`) VALUES
(11, 'Mobile shop', 'téléphonie ', 'Place Stanislas 54000 Nancy', '2015-03-30 14:36:56', '2015-03-30 14:36:56', 6, 35),
(12, 'Depan'' Info', 'Informatique', '28 rue Saint Jean Nancy', '2015-03-30 15:34:03', '2015-03-30 15:34:03', 6, 37),
(13, 'a', 'a', 'a', '2015-03-31 20:18:10', '2015-03-31 20:18:10', 7, 41);

-- --------------------------------------------------------

--
-- Structure de la table `stocks`
--

CREATE TABLE IF NOT EXISTS `stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Contenu de la table `stocks`
--

INSERT INTO `stocks` (`id`, `created`, `modified`) VALUES
(27, '2015-03-30 14:25:54', '2015-03-30 14:25:54'),
(28, '2015-03-30 14:25:55', '2015-03-30 14:25:55'),
(29, '2015-03-30 14:25:56', '2015-03-30 14:25:56'),
(30, '2015-03-30 14:35:19', '2015-03-30 14:35:19'),
(31, '2015-03-30 14:35:20', '2015-03-30 14:35:20'),
(32, '2015-03-30 14:36:00', '2015-03-30 14:36:00'),
(33, '2015-03-30 14:36:06', '2015-03-30 14:36:06'),
(34, '2015-03-30 14:36:29', '2015-03-30 14:36:29'),
(35, '2015-03-30 14:36:56', '2015-03-30 14:36:56'),
(36, '2015-03-30 15:31:10', '2015-03-30 15:31:10'),
(37, '2015-03-30 15:34:02', '2015-03-30 15:34:02'),
(38, '2015-03-31 16:11:57', '2015-03-31 16:11:57'),
(39, '2015-03-31 16:27:46', '2015-03-31 16:27:46'),
(40, '2015-03-31 20:18:04', '2015-03-31 20:18:04'),
(41, '2015-03-31 20:18:10', '2015-03-31 20:18:10'),
(42, '2015-03-31 20:19:07', '2015-03-31 20:19:07'),
(43, '2015-04-02 08:06:27', '2015-04-02 08:06:27');

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nb_articles` int(11) DEFAULT NULL,
  `type_paiement` varchar(70) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `code_rappel` int(11) DEFAULT NULL,
  `remise` decimal(5,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `pointsVente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`pointsVente_id`),
  KEY `fk_tickets_pointsventes1` (`pointsVente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=183 ;

--
-- Contenu de la table `tickets`
--

INSERT INTO `tickets` (`id`, `nb_articles`, `type_paiement`, `total`, `code_rappel`, `remise`, `created`, `modified`, `pointsVente_id`) VALUES
(159, 1, NULL, '699.00', NULL, NULL, '2015-03-01 15:15:15', '2015-03-01 15:15:15', 11),
(160, 3, NULL, '544.00', NULL, NULL, '2015-03-05 15:16:07', '2015-03-05 15:16:07', 11),
(161, 4, NULL, '160.00', NULL, NULL, '2015-03-09 15:16:19', '2015-03-09 15:16:19', 11),
(162, 5, NULL, '989.00', NULL, NULL, '2015-03-15 15:16:26', '2015-03-15 15:16:26', 11),
(163, 7, NULL, '900.00', NULL, NULL, '2015-03-19 15:16:44', '2015-03-19 15:16:44', 11),
(164, 9, NULL, '744.00', NULL, NULL, '2015-03-29 15:16:58', '2015-03-29 15:16:58', 11),
(165, 2, NULL, '1370.00', NULL, NULL, '2015-03-01 15:48:14', '2015-03-30 15:48:14', 12),
(166, 1, NULL, '59.00', NULL, NULL, '2015-03-04 15:48:52', '2015-03-30 15:48:52', 12),
(167, 3, NULL, '458.00', NULL, NULL, '2015-03-08 15:49:47', '2015-03-30 15:49:47', 12),
(168, 5, NULL, '838.00', NULL, NULL, '2015-03-12 15:49:54', '2015-03-30 15:49:54', 12),
(169, 8, NULL, '1560.00', NULL, NULL, '2015-03-15 15:50:01', '2015-03-30 15:50:01', 12),
(170, 9, NULL, '59.00', NULL, NULL, '2015-03-19 15:50:11', '2015-03-30 15:50:11', 12),
(171, 11, NULL, '1468.00', NULL, NULL, '2015-03-24 15:50:24', '2015-03-30 15:50:24', 12),
(172, 12, NULL, '969.00', NULL, NULL, '2015-03-27 15:50:29', '2015-03-30 15:50:29', 12),
(173, 13, NULL, '199.00', NULL, NULL, '2015-03-30 15:50:33', '2015-03-30 15:50:33', 12),
(174, 1, NULL, '850.00', NULL, NULL, '2015-03-30 20:11:56', '2015-03-30 20:11:56', 11),
(175, 1, NULL, '350.00', NULL, NULL, '2015-03-31 12:40:29', '2015-03-31 12:40:29', 11),
(176, 2, NULL, '989.00', NULL, NULL, '2015-03-31 12:40:45', '2015-03-31 12:40:45', 11),
(177, 1, NULL, '350.00', NULL, NULL, '2015-03-31 16:43:39', '2015-03-31 16:43:39', 11),
(178, 1, NULL, '170.00', NULL, NULL, '2015-03-31 20:49:00', '2015-03-31 20:49:00', 11),
(179, 1, NULL, '341.00', NULL, NULL, '2015-03-31 20:50:36', '2015-03-31 20:50:36', 11),
(180, 1, NULL, '341.00', NULL, NULL, '2015-03-31 20:51:55', '2015-03-31 20:51:55', 11),
(181, 2, NULL, '842.00', NULL, NULL, '2015-03-31 20:52:32', '2015-03-31 20:52:32', 11),
(182, 1, NULL, '451.08', NULL, NULL, '2015-03-31 20:54:23', '2015-03-31 20:54:23', 11);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(70) NOT NULL,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(70) NOT NULL,
  `email` varchar(70) NOT NULL,
  `password` varchar(70) NOT NULL,
  `role` varchar(70) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `nom`, `prenom`, `email`, `password`, `role`, `created`, `modified`) VALUES
(6, 'admin', 'Sam', 'admin', 'admin@caisse-co.com', '$2y$10$5t.AgWYmvai00q8lFPFyw.ARu7kejFT/wjSIBRjB8FyhN0bC2O0Lm', 'gerant', '2015-03-30 14:25:04', '2015-03-30 14:25:04'),
(7, 'test', '', 'ok', 'ok@ok.fr', '$2y$10$/C0nbDeFkY8KNq8OqKDCdepdMXsfvlmtauUDu4moiU/MsgdGP94IS', 'gerant', '2015-03-31 20:08:32', '2015-03-31 20:08:32');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `articles_stocks`
--
ALTER TABLE `articles_stocks`
  ADD CONSTRAINT `fk_stocks_has_articles_articles1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stocks_has_articles_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `articles_tickets`
--
ALTER TABLE `articles_tickets`
  ADD CONSTRAINT `fk_tickets_has_articles_articles1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tickets_has_articles_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pointsventes`
--
ALTER TABLE `pointsventes`
  ADD CONSTRAINT `fk_pointsventes_stocks1` FOREIGN KEY (`stock_id`) REFERENCES `stocks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pointsventes_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `fk_tickets_pointsventes1` FOREIGN KEY (`pointsVente_id`) REFERENCES `pointsventes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
